#pragma once
/*SeatHandler will handle seats acordingly to inserted request:
1) Book seats for ticketing system
2) Check availability of seats for purpose of booking and unbooking
3) Handle unbooking requests from ticketing system*/


#include <stdio.h>
#include <stdlib.h>

struct projection {
	int seats[12][12];
	char name[300];
	int ID = 000;
	char time[6] = "00:00";
	char lang[4] = "CZ";
	int priceA = 100;
	int priceC = 80;
}projectionEx;

void loadProjection(projection activeProjection);
void createProjection(const char* id);
